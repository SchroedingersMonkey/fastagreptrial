//fasta nextflow

nextflow.enable.dsl=2

process split_file{
    input:
      path infile
    output:
      path "${infile}.line*"
    script:
      """
      split -l 1 ${infile} -d ${infile}.line
      """
}

process start_colon{
     
    
    input:
      path infile
      val word
    output:
      path "${infile}.startCount"
    

    script:
      """
      grep -o -i "${word}" ${infile} > grepresult
      cat grepresult | wc -l > ${infile}.startCount
      """

}

/*
process end_colon{

    input:
      path infile
      val word
    output:
      path "${infile}.endCount"
    

    script:
      """
      grep -o -i "${word}" ${infile} > grepresult2
      cat grepresult | wc -l > ${infile}.endCount
      """
}



process calculate_start_total{

    publishDir "/tmp/fastaGrepTest/", mode: "copy", overwrite: true   

    input:
      path infiles
    output:
      path "startcount"
    script:
      """
      cat ${infiles} | paste -sd "+" | bc > startcount
      """

}

process calculate_end_total{

    publishDir "/tmp/fastaGrepTest/", mode: "copy", overwrite: true   

    input:
      path infiles
    output:
      path "endcount"
    script:
      """
      cat ${infiles} | paste -sd "+" | bc > startcount
      """

}

process ausgabe{

    input:
        path startfile
        path endfile
    output:
        path "startendAusgabe"
    script:
        """
        echo -n "Anzahl StartColons" > startendAusgabe
        cat ${startfile} >> startendAusgabe
        echo -n "Anzahl EndColons" >> startendAusgabe
        cat ${endfile} >> startendAusgabe
        """    

}
*/

workflow{
    inchannel = channel.fromPath("./sequences.fasta")
    splitfiles = split_file(inchannel)
    allstart = start_colon(splitfiles.flatten(), "ATG")
    //allend = end_colon(splitfiles.flatten(),"TAA")

    //startAnzahl = calculate_start_total(allstart.collect())
    //endAnzahl = calculate_end_total(allend.collect())

    //ausgabe(startAnzahl,endAnzahl)
    
}